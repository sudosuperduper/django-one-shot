from django.urls import path
from todos.views import todo_list, todo_list_detail, TodoListCreateView
from todos.views import TodoListUpdateView

# app_name = "todos"

urlpatterns = [
    path("", todo_list, name="todo_list_list"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("create/", TodoListCreateView.as_view(), name="todo_list_create"),
    path('<int:id>/edit', (), name='todo_list_update'),
]
