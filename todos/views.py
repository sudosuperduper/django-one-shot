from django.shortcuts import render, get_object_or_404
from .models import TodoList
from django.views.generic import CreateView
from django.urls import reverse_lazy
from django.views.generic.edit import UpdateView


def todo_list(request):
    todo_lists = TodoList.objects.all()
    context = {"todo_lists": todo_lists}
    return render(request, "todos/todo_list.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {"todo_list": todo_list}
    return render(request, "todos/todo_list_detail.html", context)


def todolistcreateview(request):
    model = TodoList
    fields = ["name"]
    template_name = "create_todo_list.html"
    success_url = reverse_lazy("todo_list_list")


def TodoListUpdateView(request, id):
    model = TodoList
    fields = ["name"]
    template_name = "_update_form"    
